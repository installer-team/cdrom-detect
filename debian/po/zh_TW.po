# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Traditional Chinese messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# Louies <louies0623@gmail.com>, 2019.
# Walter Cheuk <wwycheuk@gmail.com>, 2019.
# This file is distributed under the same license as debian-installer.
#
#
# Translations from iso-codes:
#   Tobias Quathamer <toddy@debian.org>, 2007.
#   Wei-Lun Chao <chaoweilun@gmail.com>, 2008, 2009.
#   Free Software Foundation, Inc., 2002, 2003
#   Alastair McKinstry <mckinstry@computer.org>, 2001,2002
#   Translations from KDE:
#   - AceLan <acelan@kde.linux.org.tw>, 2001
#   - Kenduest Lee <kenduest@i18n.linux.org.tw>, 2001
#   Tetralet <tetralet@gmail.com> 2004, 2007, 2008, 2009, 2010
#   趙惟倫 <chaoweilun@gmail.com> 2010
#   LI Daobing <lidaobing@gmail.com>, 2007.
#   Hominid He(viperii) <hominid@39.net>, 2007.
#   Mai Hao Hui <mhh@126.com>, 2001.
#   Abel Cheung <abelcheung@gmail.com>, 2007.
#   JOE MAN <trmetal@yahoo.com.hk>, 2001.
#   Chao-Hsiung Liao <j_h_liau@yahoo.com.tw>, 2005.
#   Yao Wei (魏銘廷) <mwei@lxde.org>, 2012.
#   Walter Cheuk <wwycheuk@gmail.com>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: cdrom-detect@packages.debian.org\n"
"POT-Creation-Date: 2019-09-26 22:02+0000\n"
"PO-Revision-Date: 2020-10-23 16:26+0000\n"
"Last-Translator: Walter Cheuk <wwycheuk@gmail.com>\n"
"Language-Team: Debian-user in Chinese [Big5] <debian-chinese-big5@lists."
"debian.org>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid "Load drivers from removable media?"
msgstr "是否從可移除裝置載入驅動程式？"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid "No device for installation media was detected."
msgstr "偵測不到用於安裝媒體的裝置"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid ""
"You may need to load additional drivers from removable media, such as a "
"driver floppy or a USB stick. If you have these available now, insert the "
"media, and continue. Otherwise, you will be given the option to manually "
"select some modules."
msgstr ""
"可能需要從可移除裝置 (像是驅動程式軟碟片或 USB 隨身碟) 再載入一些額外的驅動程"
"式。如果您手上已經有，請將其插入該媒體並繼續進行。否則，您將得手動來選取要載"
"入的一些模組。"

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:2001
msgid "Detecting hardware to find installation media"
msgstr "偵測硬體以找出安裝媒體"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid "Manually select a module and device for installation media?"
msgstr "是否手動選擇安裝媒體的模組及裝置？"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid "No device for installation media (like a CD-ROM device) was detected."
msgstr "沒有偵測到安裝媒體 (例如光碟裝置)。"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid ""
"If your CD-ROM drive is an old Mitsumi or another non-IDE, non-SCSI CD-ROM "
"drive, you should choose which module to load and the device to use. If you "
"don't know which module and device are needed, look for some documentation "
"or try a network installation."
msgstr ""
"若果您的光碟機是老式的 Mitsumi 或者其他非 IDE、非 SCSI 的裝置。若真是這樣的"
"話，請手動選擇它所使用的裝置及其所需載入的模組。如果您不知道應該使用哪一個裝"
"置及模組，請參考相關文件，或者使用網路安裝。"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
msgid "Retry mounting installation media?"
msgstr "是否重試掛載安裝媒體？"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
msgid ""
"Your installation media couldn't be mounted. When installing from CD-ROM, "
"this probably means that the disk was not in the drive. If so you can insert "
"it and try again."
msgstr ""
"無法掛載安裝媒體，也許是因為光碟片並未放入光碟機。若是這樣的話，請您放入光碟"
"片後再試一次。"

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
msgid "Module needed for accessing the installation media:"
msgstr "使用此安裝媒體所需載入的模組:"

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
msgid ""
"The automatic detection didn't find a drive for installation media. When "
"installing from CD-ROM and you have an unusual CD-ROM drive (that is neither "
"IDE nor SCSI), you can try to load a specific module."
msgstr ""
"自動偵測找不到安裝媒體所使用的光碟機。如果您的光碟機十分罕見 (既非 IDE 也非 "
"SCSI)，請嘗試載入其特定的驅動模組。"

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid "Device file for accessing the installation media:"
msgstr "用來存取安裝媒體的裝置檔案："

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid ""
"In order to access your installation media (like your CD-ROM), please enter "
"the device file that should be used. Non-standard CD-ROM drives use non-"
"standard device files (such as /dev/mcdx)."
msgstr ""
"為了能夠存取您的安裝媒體 (例如光碟)，請輸入其所對應的裝置檔案。非標準的光碟機"
"通常會使用非標準的裝置檔案 (像是 /dev/mcdx)。"

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid ""
"You may switch to the shell on the second terminal (ALT+F2) to check the "
"available devices in /dev with \"ls /dev\". You can return to this screen by "
"pressing ALT+F1."
msgstr ""
"您可以使用 ALT+F2 來切換到第二個終端畫面的 shell 中，並使用 ls /dev 來檢視 /"
"dev 目錄下可用的裝置檔。請再按下 ALT+F1 來返回本畫面。"

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:10001
msgid "Scanning installation media"
msgstr "掃描安裝媒體"

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:11001
msgid "Scanning ${DIR}..."
msgstr "正在掃瞄 ${DIR}……"

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
msgid "Installation media detected"
msgstr "偵測到安裝媒體"

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
msgid ""
"Autodetection of the installation media was successful. A drive has been "
"found that contains '${cdname}'. The installation will now continue."
msgstr ""
"成功偵測到安裝媒體。找到了光碟機，而且裡面所放置的光碟是 ${cdname}。安裝作業"
"現可繼續進行。"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid "UNetbootin media detected"
msgstr "偵測到 UNetbootin 媒體"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"It appears that your installation medium was generated using UNetbootin. "
"UNetbootin is regularly linked with difficult or unreproducible problem "
"reports from users; if you have problems using this installation medium, "
"please try your installation again without using UNetbootin before reporting "
"issues."
msgstr ""
"看起來您的開機媒體是使用 UNetbootin 製作的。我們經常收到難以或無法重現的 "
"UNetbootin 使用者回報。如果您在安裝時出現問題，在回報問題之前，請嘗試不要使"
"用 UNetbootin 進行安裝媒體製作。"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"The installation guide contains more information on how to create a USB "
"installation medium directly without UNetbootin."
msgstr "安裝說明中包含如何不透過 UNetbootin 製作 USB 安裝媒體的使用說明。"

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "Incorrect installation media detected"
msgstr "偵測到不對的安裝媒體"

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "The detected media cannot be used for installation."
msgstr "所放入的安裝媒體不能用於安裝。"

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "Please provide suitable media to continue with the installation."
msgstr "請提供適當的媒體，來令安裝作業能繼續進行。"

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid "Error reading Release file"
msgstr "在讀取 Release 檔時發生錯誤"

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid ""
"The installation media do not seem to contain a valid 'Release' file, or "
"that file could not be read correctly."
msgstr ""
"在這個安裝媒體裡似乎並沒有包含正確的 'Release' 檔，或是這個檔案無法正確讀取。"

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid ""
"You may try to repeat the media detection, but even if it does succeed the "
"second time, you may experience problems later in the installation."
msgstr ""
"可以再次試著偵測安裝媒體，但即使第二次僥倖成功了，在之後的安裝過程裡還是可能"
"會遇到問題。"

#. Type: text
#. Description
#. finish-install progress bar item
#. :sl1:
#: ../cdrom-detect.templates:19001
msgid "Unmounting/ejecting installation media..."
msgstr "卸除/退出安裝媒體..."

#. Type: text
#. Description
#. Item in the main menu to select this package
#. Translators: keep below 55 columns.
#. :sl2:
#: ../cdrom-detect.templates:20001
msgid "Detect and mount installation media"
msgstr "偵測並掛載安裝媒體"
