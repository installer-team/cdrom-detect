# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of ru.po to Russian
# Russian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
#
# Translations from iso-codes:
# Russian L10N Team <debian-l10n-russian@lists.debian.org>, 2004.
# Yuri Kozlov <yuray@komyakino.ru>, 2004, 2005.
# Dmitry Beloglazov <dm-guest@alioth.debian.org>, 2005.
# Sergey Alyoshin <alyoshin.s@gmail.com>, 2011.
# Yuri Kozlov <yuray@komyakino.ru>, 2005, 2006, 2007, 2008.
# Yuri Kozlov <yuray@komyakino.ru>, 2009, 2010, 2011.
# Alastair McKinstry <mckinstry@debian.org>, 2004.
# Mikhail Zabaluev <mhz@altlinux.org>, 2006.
# Nikolai Prokoschenko <nikolai@prokoschenko.de>, 2004.
# Pavel Maryanov <acid@jack.kiev.ua>, 2009,2010.
# Yuri Kozlov <yuray@komyakino.ru>, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2018, 2020.
# Lev Lamberov <dogsleg@debian.org>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: ru\n"
"Report-Msgid-Bugs-To: cdrom-detect@packages.debian.org\n"
"POT-Creation-Date: 2019-09-26 22:02+0000\n"
"PO-Revision-Date: 2020-11-16 07:09+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid "Load drivers from removable media?"
msgstr "Загрузить драйверы со сменного носителя?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid "No device for installation media was detected."
msgstr "Устройство для установочного носителя не обнаружено."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid ""
"You may need to load additional drivers from removable media, such as a "
"driver floppy or a USB stick. If you have these available now, insert the "
"media, and continue. Otherwise, you will be given the option to manually "
"select some modules."
msgstr ""
"Возможно, вам понадобится подгрузить дополнительные драйверы со сменного "
"носителя, например с дискет или USB. Если такой носитель у вас под рукой, "
"вставьте его и продолжайте. Если это не так -- у вас будет возможность "
"вручную выбрать из нескольких модулей."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:2001
msgid "Detecting hardware to find installation media"
msgstr "Обнаружение устройств с установочным носителем"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid "Manually select a module and device for installation media?"
msgstr "Выбрать модуль и устройство для установочного носителя вручную?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid "No device for installation media (like a CD-ROM device) was detected."
msgstr ""
"Устройство для установочного носителя (например, привод CD-ROM) не "
"обнаружено."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid ""
"If your CD-ROM drive is an old Mitsumi or another non-IDE, non-SCSI CD-ROM "
"drive, you should choose which module to load and the device to use. If you "
"don't know which module and device are needed, look for some documentation "
"or try a network installation."
msgstr ""
"Если вы используете старое устройство чтения Mitsumi или другой не-IDE, не-"
"SCSI CD-ROM, то в таком случае вы должны указать, какие модули загрузить и "
"какое устройство использовать. Если вы не знаете какие модули и устройства "
"вам нужны, то почитайте документацию или попробуйте установку по сети."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
msgid "Retry mounting installation media?"
msgstr "Повторить монтирование установочного носителя?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
msgid ""
"Your installation media couldn't be mounted. When installing from CD-ROM, "
"this probably means that the disk was not in the drive. If so you can insert "
"it and try again."
msgstr ""
"Не удаётся смонтировать установочный носитель. Возможно, он не вставлен в "
"привод CD-ROM. Если это действительно так, вставьте его и попробуйте ещё раз."

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
msgid "Module needed for accessing the installation media:"
msgstr "Модуль, необходимый для доступа к установочному носителю:"

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
msgid ""
"The automatic detection didn't find a drive for installation media. When "
"installing from CD-ROM and you have an unusual CD-ROM drive (that is neither "
"IDE nor SCSI), you can try to load a specific module."
msgstr ""
"Определить привод установочного носителя автоматически не удаётся. Если у "
"вас необычный привод CD-ROM (не IDE и не SCSI), то попробуйте загрузить "
"подходящий модуль."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid "Device file for accessing the installation media:"
msgstr "Файл устройства для доступа к установочному носителю:"

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid ""
"In order to access your installation media (like your CD-ROM), please enter "
"the device file that should be used. Non-standard CD-ROM drives use non-"
"standard device files (such as /dev/mcdx)."
msgstr ""
"Для доступа к установочному носителю (например, приводу CD-ROM) укажите имя "
"файла устройства. Для нестандартных CD-ROM используются нестандартные имена "
"файлов устройств (например, /dev/mcdx)."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid ""
"You may switch to the shell on the second terminal (ALT+F2) to check the "
"available devices in /dev with \"ls /dev\". You can return to this screen by "
"pressing ALT+F1."
msgstr ""
"Вы можете переключиться в оболочку на второй консоли (ALT+F2) для проверки "
"доступных устройств в каталоге /dev командой \"ls /dev\". Назад вы можете "
"вернуться нажатием ALT+F1."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:10001
msgid "Scanning installation media"
msgstr "Сканирование установочного носителя"

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:11001
msgid "Scanning ${DIR}..."
msgstr "Выполняется поиск в ${DIR}..."

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
msgid "Installation media detected"
msgstr "Обнаружен установочный носитель"

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
msgid ""
"Autodetection of the installation media was successful. A drive has been "
"found that contains '${cdname}'. The installation will now continue."
msgstr ""
"Автоматическое определение установочного носителя прошло успешно. Обнаружен "
"привод с ${cdname}. Установка будет продолжена."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid "UNetbootin media detected"
msgstr "Обнаружен носитель с UNetbootin"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"It appears that your installation medium was generated using UNetbootin. "
"UNetbootin is regularly linked with difficult or unreproducible problem "
"reports from users; if you have problems using this installation medium, "
"please try your installation again without using UNetbootin before reporting "
"issues."
msgstr ""
"Кажется, что ваш носитель программы установки сгенерирован с помощью "
"UNetbootin. При использовании UNetbootin, по сообщениям пользователей, "
"возникают сложности или неразрешимые проблемы; если у вас возникли проблемы "
"при использовании этого носителя, то перед тем как сообщать о ней, "
"попробуйте выполнить установку ещё раз, без использования UNetbootin."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"The installation guide contains more information on how to create a USB "
"installation medium directly without UNetbootin."
msgstr ""
"В руководстве по установке есть подробное описание о том, как создать "
"установочный носитель USB без UNetbootin."

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "Incorrect installation media detected"
msgstr "Обнаружен некорректный установочный носитель"

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "The detected media cannot be used for installation."
msgstr "Обнаруженный носитель не может быть использован для установки."

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "Please provide suitable media to continue with the installation."
msgstr "Чтобы продолжить установку вставьте правильный носитель."

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid "Error reading Release file"
msgstr "Ошибка чтения файла Release"

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid ""
"The installation media do not seem to contain a valid 'Release' file, or "
"that file could not be read correctly."
msgstr ""
"Вероятно, установочный носитель не содержит правильного файла 'Release' или "
"этот файл был неправильно прочитан."

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid ""
"You may try to repeat the media detection, but even if it does succeed the "
"second time, you may experience problems later in the installation."
msgstr ""
"Вы можете попробовать повторить процедуру обнаружения носителя, но даже если "
"со второго раза она пройдёт успешно, у вас могут возникнуть проблемы во "
"время установки."

#. Type: text
#. Description
#. finish-install progress bar item
#. :sl1:
#: ../cdrom-detect.templates:19001
msgid "Unmounting/ejecting installation media..."
msgstr "Размонтирование/извлечение установочного носителя…"

#. Type: text
#. Description
#. Item in the main menu to select this package
#. Translators: keep below 55 columns.
#. :sl2:
#: ../cdrom-detect.templates:20001
msgid "Detect and mount installation media"
msgstr "Поиск и монтирование установочного носителя"
