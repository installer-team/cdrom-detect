# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Norwegian Nynorsk translation of debian-installer.
# Copyright (C) 2003–2010 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Håvard Korsvoll <korsvoll@skulelinux.no>, 2004, 2005, 2006, 2007, 2008.
# Eirik U. Birkeland <eirbir@gmail.com>, 2010.
# Allan Nordhøy <epost@anotheragency.no>, 2017.
# Alexander Jansen <bornxlo@gmail.com>, 2018.
# aujawindar <eivind.odegard@sogn.no>, 2022.
#
# Translations from iso-codes:
#   Alastair McKinstry <mckinstry@computer.org>, 2001.
#   Free Software Foundation, Inc., 2001, 2004.
#   Håvard Korsvoll <korsvoll@gmail.com>, 2004,2006, 2007.
#   Karl Ove Hufthammer <karl@huftis.org>, 2003-2004, 2006. (New translation done from scratch.).
#   Kjartan Maraas  <kmaraas@gnome.org>, 2001.
#   Roy-Magne Mo <rmo@sunnmore.net>, 2001.
#   Tobias Quathamer <toddy@debian.org>, 2007.
#     Translations taken from ICU SVN on 2007-09-09
msgid ""
msgstr ""
"Project-Id-Version: nn\n"
"Report-Msgid-Bugs-To: cdrom-detect@packages.debian.org\n"
"POT-Creation-Date: 2019-09-26 22:02+0000\n"
"PO-Revision-Date: 2023-05-17 23:53+0000\n"
"Last-Translator: Yngve Spjeld-Landro <l10n@landro.net>\n"
"Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid "Load drivers from removable media?"
msgstr "Lasta inn drivarar frå eit eksternt lagringsmedium?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid "No device for installation media was detected."
msgstr "Fann inga eining for installasjonsmediet."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid ""
"You may need to load additional drivers from removable media, such as a "
"driver floppy or a USB stick. If you have these available now, insert the "
"media, and continue. Otherwise, you will be given the option to manually "
"select some modules."
msgstr ""
"Det kan vere du må laste inn drivarar frå eit ekstern lagringsmedium, slik "
"som ein diskett eller ein minnepinne. Viss du har eit slikt lagringsmedium "
"tilgjengeleg no, set det inn og gå vidare. Viss ikkje så vil du bli gjeve "
"høve til å velje CDROM-modular manuelt."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:2001
msgid "Detecting hardware to find installation media"
msgstr "Søkjer gjennom maskinvara for å finne installasjonsmedium"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid "Manually select a module and device for installation media?"
msgstr "Vil du velje modulen og eininga for installasjonsmediet manuelt?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid "No device for installation media (like a CD-ROM device) was detected."
msgstr "Fann inga eining for installasjonsmediet (slik som ein CD-stasjon)."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid ""
"If your CD-ROM drive is an old Mitsumi or another non-IDE, non-SCSI CD-ROM "
"drive, you should choose which module to load and the device to use. If you "
"don't know which module and device are needed, look for some documentation "
"or try a network installation."
msgstr ""
"Viss du har ein gammal Mitsumi-spelar eller ein annan CDROM-spelar som ikkje "
"er av IDE- eller SCSI-type, bør du velje kva for modul og eining som skal "
"brukast. Dersom du ikkje veit kva for modul og eining som trengst, bør du "
"prøve å finne dokumentasjon ein annan stad eller prøve ein nettinstallasjon.."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
msgid "Retry mounting installation media?"
msgstr "Vil du prøva å montera installasjonsmediet ein gong til?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
msgid ""
"Your installation media couldn't be mounted. When installing from CD-ROM, "
"this probably means that the disk was not in the drive. If so you can insert "
"it and try again."
msgstr ""
"Klarte ikkje montera installasjonsmediet. Viss du installerer frå CD-ROM, er "
"det truleg på grunn av at CD-en ikkje var i stasjonen. I so fall kan du "
"setje han i og prøve på ny."

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
msgid "Module needed for accessing the installation media:"
msgstr "Modulen som trengst for å få tilgang til installasjonsmediet:"

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
msgid ""
"The automatic detection didn't find a drive for installation media. When "
"installing from CD-ROM and you have an unusual CD-ROM drive (that is neither "
"IDE nor SCSI), you can try to load a specific module."
msgstr ""
"Installasjonsprogrammet fann inga eining for installasjonsmediet. Viss du "
"installerer frå CD-ROM og har ein uvanleg CD-stasjon (som korkje er IDE "
"eller SCSI), kan du prøva å lasta ein spesifikk modul."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid "Device file for accessing the installation media:"
msgstr "Einingsfil for å få tilgang til installasjonsmediet:"

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid ""
"In order to access your installation media (like your CD-ROM), please enter "
"the device file that should be used. Non-standard CD-ROM drives use non-"
"standard device files (such as /dev/mcdx)."
msgstr ""
"For å kunna bruka installasjonsmediet (som CD-ROM), må du skriva inn kva for "
"einingsfil som skal brukast. Uvanlege CDROM-spelarar brukar uvanlege "
"einingsfiler, som til dømes /dev/mcdx."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid ""
"You may switch to the shell on the second terminal (ALT+F2) to check the "
"available devices in /dev with \"ls /dev\". You can return to this screen by "
"pressing ALT+F1."
msgstr ""
"Du kan byta til skalet på den andre konsollen (Alt + F2) for å sjå kva for "
"einingar som finst i /dev med kommandoen «ls /dev». Du kan gå tilbake hit "
"med Alt + F1."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:10001
msgid "Scanning installation media"
msgstr "Ser gjennom installasjonsmediet"

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:11001
msgid "Scanning ${DIR}..."
msgstr "Søkjer gjennom ${DIR} …"

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
msgid "Installation media detected"
msgstr "Fann eit installasjonsmedium"

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
msgid ""
"Autodetection of the installation media was successful. A drive has been "
"found that contains '${cdname}'. The installation will now continue."
msgstr ""
"Oppdaga installasjonsmediet automatisk. Du har sett inn ein stasjon som "
"inneheld '${cdname}'. Installasjonen vil halda fram."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid "UNetbootin media detected"
msgstr "Fann UNetbootin-medium"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"It appears that your installation medium was generated using UNetbootin. "
"UNetbootin is regularly linked with difficult or unreproducible problem "
"reports from users; if you have problems using this installation medium, "
"please try your installation again without using UNetbootin before reporting "
"issues."
msgstr ""
"Det ser ut til at installasjonsmediet ditt blei generert med UNetbootin. "
"UNetbootin er ofte knytt til vanskelege eller ikkje-reproduserbare "
"feilrapportar frå brukarar. Viss du har problem med å bruke dette "
"installasjonsmediet, prøv å installere igjen utan å bruka UNetbootin før du "
"rapporterer feil."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"The installation guide contains more information on how to create a USB "
"installation medium directly without UNetbootin."
msgstr ""
"Installasjonsrettleiaren inneheld meir informasjon om korleis opprette eit "
"USB-installasjonsmedium direkte utan UNetbootin."

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "Incorrect installation media detected"
msgstr "Fann feil installasjonsmedium"

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "The detected media cannot be used for installation."
msgstr "Dette mediet kan ikkje brukast til installasjon."

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "Please provide suitable media to continue with the installation."
msgstr "Set inn rett installasjonsmedium for å halda fram med installasjonen."

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid "Error reading Release file"
msgstr "Feil under lesing av Release-fila"

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid ""
"The installation media do not seem to contain a valid 'Release' file, or "
"that file could not be read correctly."
msgstr ""
"Installasjonsmediet inneheld ikkje ei gyldig «Release»-fil, eller ho kunne "
"ikkje lesast rett."

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid ""
"You may try to repeat the media detection, but even if it does succeed the "
"second time, you may experience problems later in the installation."
msgstr ""
"Du kan gjera eit nytt forsøk på å oppdaga installasjonsmediet, men sjølv om "
"det lukkast andre gongen, kan du få problem seinare i installeringa."

#. Type: text
#. Description
#. finish-install progress bar item
#. :sl1:
#: ../cdrom-detect.templates:19001
msgid "Unmounting/ejecting installation media..."
msgstr "Avmonterer / løyser ut installasjonsmediet …"

#. Type: text
#. Description
#. Item in the main menu to select this package
#. Translators: keep below 55 columns.
#. :sl2:
#: ../cdrom-detect.templates:20001
msgid "Detect and mount installation media"
msgstr "Finn og monter installasjonsmedium"
